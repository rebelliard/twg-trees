# TWG Trees

* [me@rebelliard.com](mailto:me@rebelliard.com?subject=[TWG])
* [rebelliard.com](https://rebelliard.com)

## Installation instructions

* On MacOS:

1. Install [Homebrew](http://brew.sh).
2. Install Python:

        brew install python

* On Ubuntu:

        sudo apt-get install python

## Running the test suite
The program has been written for Python 2 and the test suite has been built with its native
testing framework.

    python tests.py

## Problem
We've got a interesting problem on one of our projects. We keep a nested tree of unlimited depth of categories, and their children. We want to expose this data via a REST API, but our client application needs to show a flat list of category links, sorted alphabetically at every depth.

Have you ever encountered a situation like this before? What would you do?

### Example
We might have some categories stored in the DB as follows:

    (Category)      (Parent)
    "Family"        null
    "Parenting"     "Family"
    "Caregiving"    "Family"
    "Finances"      "Family"
    "RRSPs"         "Finances"
    "Savings"       "Finances"
    "Planning"      "Finances"
    "Career"        null
    "Managers"      "Career"
    "Employees"     "Career"

The response from the API should return a list of flattened categories in the following order:

    "Career"
    "Employees"
    "Managers"
    "Family"
    "Caregiving"
    "Finances"
    "Planning"
    "RRSPs"
    "Savings"
    "Parenting"

### Assignment
Create a function (or multiple functions) that accepts a list of Category/Parent objects and returns a list of strings, as described in the problem above.
