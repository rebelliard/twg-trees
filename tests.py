import unittest
import twg

class FlattenCategoriesTest(unittest.TestCase):

    def test_build_tree(self):
        tuples = [
            ('Family',      None),
            ('Parenting',   'Family'),
            ('Caregiving',  'Family'),
            ('Finances',    'Family'),
            ('RRSPs',       'Finances'),
            ('Savings',     'Finances'),
            ('Planning',    'Finances'),
            ('Career',      None),
            ('Managers',    'Career'),
            ('Employees',   'Career'),
        ]
        result = [
            [
                (None, 'Family'),
                (None, 'Career'),
            ],
            [
                ('Career', 'Managers'),
                ('Career', 'Employees'),
                ('Family', 'Parenting'),
                ('Family', 'Caregiving'),
                ('Family', 'Finances'),
            ],
            [
                ('Finances', 'RRSPs'),
                ('Finances', 'Planning'),
                ('Finances', 'Savings')
            ]
        ]
        self.assertEqual(twg.build_tree(tuples), result)

    def test_flatten_tree(self):
        tree = [
            [
                (None, 'Family'),
                (None, 'Career'),
            ],
            [
                ('Family', 'Parenting'),
                ('Career', 'Managers'),
                ('Career', 'Employees'),
                ('Family', 'Caregiving'),
                ('Family', 'Finances'),
            ],
            [
                ('Finances', 'RRSPs'),
                ('Finances', 'Planning'),
                ('Finances', 'Savings'),
            ],
        ]

        result = [
            'Career',
            'Employees',
            'Managers',
            'Family',
            'Caregiving',
            'Finances',
            'Planning',
            'RRSPs',
            'Savings',
            'Parenting',
        ]
        self.assertEqual(twg.flatten_tree(tree), result)

    def test_get_categories(self):

        tuples = [
            ('Family',      None),
            ('Parenting',   'Family'),
            ('Caregiving',  'Family'),
            ('Finances',    'Family'),
            ('RRSPs',       'Finances'),
            ('Savings',     'Finances'),
            ('Planning',    'Finances'),
            ('Career',      None),
            ('Managers',    'Career'),
            ('Employees',   'Career'),
        ]

        result = [
            'Career',
            'Employees',
            'Managers',
            'Family',
            'Caregiving',
            'Finances',
            'Planning',
            'RRSPs',
            'Savings',
            'Parenting',
        ]
        self.assertEqual(twg.get_categories(tuples), result)

if __name__ == '__main__':
    unittest.main()
