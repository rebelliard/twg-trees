"""
Assignment:
    Create a function (or multiple functions) that accepts a list of Category/Parent
    objects and returns a list of strings, as described in the README file.
"""
import sys
import math
from operator import itemgetter

def build_tree(tuples):
    '''
    Build a grouped array that can be more easily traversed than a flat duple.
    '''
    tree = {}

    # Build an indexed tuple.
    for children, parent in tuples:
        if parent is None:
            # node: (depth, parent)
            tree[children] = (0, None)
        else:
            node_depth, node_parent = tree.get(parent, (0, None))
            depth = node_depth + 1
            tree[children] = (depth, parent)

    def tree_to_array(tree):
        '''
        Build the actual grouped array off of the indexed tuple.
        '''
        items = sorted(tree.iteritems(), key=itemgetter(1))
        last_depth_item = items[-1]
        last_node, (max_depth, last_parent) = last_depth_item

        # Create the maximum depth's containers.
        arr = [[] for x in range(max_depth + 1)]

        for node, (depth, parent) in items:
            arr[depth].append((parent, node))
        return arr

    arr = tree_to_array(tree)
    return arr


def flatten_tree(tree, start=None, node_parent=None):
    '''
    Recursively go through the list of (category, parent) tuples
    and flatten them out.
    '''

    # Sort by (parent, node).
    # - `itemgetter` is more memory efficient than lambda operators on potentially large loops.
    # - See http://stackoverflow.com/questions/17243620/operator-itemgetter-or-lambda
    sorting_key = itemgetter(0, 1)

    start = start or 0
    max_depth = len(tree)
    flattened = []
    if max_depth > start:
        # Virtually slice the array and look for potential children.
        nodes = sorted(tree[start], key=sorting_key)
        for parent, node in nodes:
            if parent == node_parent:
                # We found a node!
                flattened.append(node)
                # Now let's try to find any children by recursively going
                # through the following groups and using the current node
                # as our parent.
                flattened += flatten_tree(tree, start + 1, node_parent = node)
    return flattened

def get_categories(tuples):
    categories = flatten_tree(build_tree(tuples))
    return categories
